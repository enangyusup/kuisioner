-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 26, 2012 at 08:53 AM
-- Server version: 5.5.21
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kuisioner_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `kuis`
--

CREATE TABLE IF NOT EXISTS `kuis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan_id` int(11) NOT NULL,
  `responden_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `master_telpon`
--

CREATE TABLE IF NOT EXISTS `master_telpon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_telp` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `master_telpon`
--

INSERT INTO `master_telpon` (`id`, `no_telp`, `nama`) VALUES
(1, '087838203610', 'Enang Yusup'),
(2, '09839883838', 'Ujang Ujangan'),
(3, '984948484939', 'Didin Hidayat');

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan`
--

CREATE TABLE IF NOT EXISTS `pertanyaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `soal` varchar(200) NOT NULL,
  `ket` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `pertanyaan`
--

INSERT INTO `pertanyaan` (`id`, `soal`, `ket`) VALUES
(1, 'Politik', 'Pertanyaan tentang politik1'),
(3, 'Budaya1', 'Pertanyaan seputar budaya');

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan_detail`
--

CREATE TABLE IF NOT EXISTS `pertanyaan_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `soal` varchar(200) NOT NULL,
  `ket` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `pertanyaan_detail`
--

INSERT INTO `pertanyaan_detail` (`id`, `p_id`, `soal`, `ket`) VALUES
(2, 1, 'Stasiun Balapan', 'Aduh naun nya'),
(3, 3, 'Naun Cing', 'Haha'),
(4, 3, 'Aduh repot', 'Aduh '),
(5, 3, 'tah geningan', 'tah genig'),
(6, 1, 'Bang', 'Ban');

-- --------------------------------------------------------

--
-- Table structure for table `responden`
--

CREATE TABLE IF NOT EXISTS `responden` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_id` int(11) NOT NULL,
  `usia` int(11) NOT NULL,
  `pendidikan` enum('TS','SD','SMP','SMA','D1','D2','D3','S1','S2','S3') NOT NULL DEFAULT 'SD',
  `status` enum('Y','N') NOT NULL DEFAULT 'N',
  `jum_anak` tinyint(3) NOT NULL,
  `ket` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `responden`
--

INSERT INTO `responden` (`id`, `master_id`, `usia`, `pendidikan`, `status`, `jum_anak`, `ket`) VALUES
(6, 1, 1, 'TS', 'Y', 1, '');

<?php $this->load->view('inc/header') ?>

<div id="content">
    <div class="scroll">
        <div class="scrollContainer">

<!-- for delete !-->




<div class="panel">
    <h1><?php echo empty($id) ? 'Input' : 'Edit' ?> Responden </h1>

    <form method="post" name="myform" action="">

    <div class="col_320 float_l">
        <label class="label" for="No.Telp">No Telp:</label>
        <?php
        echo form_hidden('master_id', $row['master_id']);
        $data = array(
                'name'        => 'no_telp',
                'id'          => 'no_telp',
                'value'       => $row['no_telp'],
                'style'       => 'width:108px',
                'class'       => 'input',
                'disabled'    => 'disabled',
        );
        echo form_input($data);
        ?>
        <div class="cleaner_h10"></div>

        <label class="label" for="nama">Nama:</label>
        <?php
        $data = array(
                'name'        => 'nama',
                'id'          => 'nama',
                'value'       => $row['nama'],
                'style'       => 'width:108px',
                'class'       => 'input',
                'disabled'    => 'disabled',
        );
        echo form_input($data);
        ?>
        <div class="cleaner_h10"></div>

        <label class="label" for="usia">Usia:</label>
        <?php
        $data = array(
                'name'        => 'usia',
                'id'          => 'usia',
                'value'       => $row['usia'],
                'maxlength'   => '10',
                'style'       => 'width:30px',
                'class'       => 'input',
        );
        echo form_input($data);
        ?>
        <div class="cleaner_h10"></div>

        <label class="label" for="pendidikan">Pendidikan:</label>
<?php
		$options = array (
			 'TS' => 'Tidak Sekolah',
			 'SD' => 'SD',
			 'SMP' => 'SMP',
			 'SMA' => 'SMA',
			 'D1' => 'D1',
			 'D2' => 'D2',
			 'D3' => 'D3',
			 'S1' => 'S1',
			 'S2' => 'S2',
			 'S3' => 'S3'
		);
        $current = $row['pendidikan'];
        echo form_dropdown('pendidikan', $options, $current, 'id="pendidikan" class="select"');
?>
        <div class="cleaner"></div>

    </div>

    <div class="col_320 float_r">
        <label class="label" for="status">Sudah Pernikahan:</label>
        <?php
            $current = $row['status'] == 'Y' ? TRUE : FALSE;
            $data = array(
                    'name'     => 'status',
                    'id'       => 'status1',
                    'value'    => 'Y',
                    'checked'  => $current,
            );
            echo form_radio($data);
            echo form_label('Ya', 'status1');

            $current = $row['status'] == 'N' ? TRUE : FALSE;
            $data = array(
                    'name'     => 'status',
                    'id'       => 'status2',
                    'value'    => 'N',
                    'checked'  => $current,
            );
            echo form_radio($data);
            echo form_label('Tidak', 'status2');
        ?>
        <div class="cleaner_h10"></div>
        <label class="label" for="anak">Jumlah Anak:</label>
        <?php
        $data = array(
                'name'        => 'anak',
                'id'          => 'anak',
                'value'       => $row['jum_anak'],
                'maxlength'   => '10',
                'style'       => 'width:30px',
                'class'       => 'input',
        );
        echo form_input($data);
        ?>
        <div class="cleaner"></div>
        <label class="label" for="ket">Keterangan:</label>
        <?php
        $data = array(
                'name'        => 'ket',
                'id'          => 'ket',
                'value'       => $row['ket'],
                'class'       => 'textarea',
        );
        echo form_textarea($data);
        ?>

        <div class="cleaner_h10"></div>
    </div>
    <div class="cleaner_h20"></div>
    <div style="text-align: center">
        <?php echo form_error('anak'); ?>
        <?php echo form_error('usia'); ?>
        <input type="submit" class="submit_btn" name="submit" id="submit" value="Send" />
        <input type="reset" class="submit_btn" name="reset" id="reset" value="Reset" />
    </div>
    </form>
</div>



<!-- for delete !-->
        </div>
    </div>
</div>

<?php $this->load->view('inc/footer') ?>

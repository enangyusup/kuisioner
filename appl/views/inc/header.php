<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kuisioner - <?php echo isset($title) ? $title : '' ?></title>

<?php
    echo css_asset('styles.css');
    echo css_asset('mix.css');
    echo css_asset('coda-slider.css', NULL, array('media'=>'screen', 'charset'=>'utf-8'));
    echo js_asset('jquery-1.2.6.js');
    echo js_asset('jquery.scrollTo-1.3.3.js');
    echo js_asset('jquery.localscroll-1.2.5.js');
    echo js_asset('jquery.serialScroll-1.2.1.js');
    //echo js_asset('coda-slider.js');
    echo js_asset('jquery.easing.1.3.js');
?>

</head>

<body>

    <div id="slider">
    <div id="templatemo_wrapper">

        <div id="header">

            <h1><a href="<?=base_url()?>">Kuisioner<span>Free CSS Template</span></a></h1>
        </div>

        <div id="menu">
            <ul class="navigation">
                <li><a href="<?= site_url() ?>" class="selected">Home<span class="ui_icon home"></span></a></li>
                <li><a href="<?= site_url('pertanyaan') ?>">Master<span class="ui_icon aboutus"></span></a></li>
                <li><a href="<?= site_url('responden') ?>">Responden<span class="ui_icon services"></span></a></li>
                <li><a href="<?= site_url('kuis') ?>">Kuisioner<span class="ui_icon gallery"></span></a></li>
                <li><a href="#contactus">Contact Us<span class="ui_icon contactus"></span></a></li>
            </ul>
        </div>

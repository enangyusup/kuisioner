<?php $this->load->view('inc/header') ?>



        <div id="content">

            <div class="scroll">
                <div class="scrollContainer">

                    <div class="panel" id="gallery">
                        <h1>Web Templates Gallery</h1>

                        <div id="gallery_container">
                            <div class="gallery_box">
                                <img src="images/gallery/image_01.jpg" alt="01" />
                                <h4>Project Title 1</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nec ligula vitae ipsum blandit condimentum. Nam fringilla luctus mauris, non ornare turpis lobortin.</p>
                                 <div class="btn_more"><a href="#">Visit <span>&raquo;</span></a></div>
                                <div class="cleaner"></div>
                            </div>
                            <div class="gallery_box">
                                <img src="images/gallery/image_02.jpg" alt="02" />
                                <h4>Project Title 2</h4>
                                <p>Donec ac eros ac nunc blandit hendrerit. Vestibulum tincidunt, odio at ultricies sollicitudin, ante felis luctus justo, non venenatis quam mauris non tortor.</p>
                                 <div class="btn_more"><a href="#">Visit <span>&raquo;</span></a></div>
                                <div class="cleaner"></div>
                            </div>
                            <div class="gallery_box">
                                <img src="images/gallery/image_03.jpg" alt="03" />
                                <h4>Project Title 3</h4>
                                <p> Mauris ligula tortor, congue laoreet rutrum eget, suscipit nec augue. In congue consectetur est, sit amet hendrerit velit adipiscing eget.</p>
                                 <div class="btn_more"><a href="#">Visit <span>&raquo;</span></a></div>
                                <div class="cleaner"></div>
                            </div>
                            <div class="gallery_box">
                                <img src="images/gallery/image_04.jpg" alt="04" />
                                <h4>Project Title 4</h4>
                                <p> Curabitur iaculis, erat pharetra porttitor sollicitudin, turpis lectus placerat arcu, ac mattis eros sem ut metus. Nunc congue iaculis lectus in interdum.</p>
                                 <div class="btn_more"><a href="#">Visit <span>&raquo;</span></a></div>
                                <div class="cleaner"></div>
                            </div>
                            <div class="gallery_box">
                                <img src="images/gallery/image_05.jpg" alt="05" />
                                <h4>Project Title 5</h4>
                                <p> Curabitur iaculis enim dolor. Sed quis augue ligula. Quisque aliquet egestas felis, in egestas turpis sodales et. In ac diam ut orci viverra bibendum. </p>
                                 <div class="btn_more"><a href="#">Visit <span>&raquo;</span></a></div>
                                <div class="cleaner"></div>
                            </div>
                            <div class="gallery_box">
                                <img src="images/gallery/image_06.jpg" alt="06" />
                                <h4>Project Title 6</h4>
                                <p>Sed in viverra nulla. Duis rutrum vehicula ligula, non tempor nunc congue et. Nunc sit amet tortor nulla, ut eleifend enim sed condimentum tellus vestibulum in.</p>
                                 <div class="btn_more"><a href="#">Visit <span>&raquo;</span></a></div>
                                <div class="cleaner"></div>
                            </div>
                            <div class="cleaner"></div>
                        </div>

                    </div>

                    <div class="panel" id="contactus">
                    	<h1>Contact Us Now!</h1>

                        <div class="col_320 float_l">
                            <div id="contact_form">

                                <form method="post" name="contact" action="#contactus">

                                    <label for="author">Name:</label> <input name="author" type="text" class="input_field" id="author" maxlength="60" />
                                    <div class="cleaner_h10"></div>

                                    <label for="email">Email:</label> <input name="email" type="text" class="input_field" id="email" maxlength="60" />
                                    <div class="cleaner_h10"></div>

                                    <label for="text">Message:</label> <textarea id="text" name="text" rows="0" cols="0" class="required"></textarea>
                                    <div class="cleaner_h10"></div>

                                    <input type="submit" class="submit_btn float_l" name="submit" id="submit" value="Send" />
                                    <input type="reset" class="submit_btn float_r" name="reset" id="reset" value="Reset" />

                           		</form>

                            </div>
						</div>

                        <div class="col_320 float_r">

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Suspendisse in lectus turpis. Vivamus cursus tortor quis leo ullamcorper auctor quis tincidunt metus.</p>

                        	<h4>Our Address</h4>

                            <h6>Web Design Company</h6>
                            110-160 Vitae urna blandit est egestas, <br />
                            Pulvinar sit amet convallis eget, 20180<br />
                            Lorem ipsum dolor<br />

                			<br />
                     		Tel: 020-010-7800<br />
                        	Fax: 020-010-6800
                        </div>
                	</div>

                </div>
            </div>

            <!-- end of scroll -->

        </div> <!-- end of content -->

<? /*
<div id="content">
<h1>List Master Telpon</h1>
    <table border=1>
        <tr>
            <th>No</th>
            <th>No Telp</th>
        </tr>
    <?php $no=0; foreach($list_master as $k=>$v) : $no++; ?>
        <tr>
            <td><?=$no?></td>
            <td><?=$v['no_telp']?></td>
        </tr>
    <?php endforeach ?>
    </table>
    <a href="<?=site_url('main/random')?>">Get Random</a>
</div>

   */ ?>
<?php $this->load->view('inc/footer') ?>

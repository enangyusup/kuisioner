<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class n_paginate {

    var $CI;

    var $panel   = "#panel";
    var $page    = 1;
    var $url     = NULL;
    var $totRec  = 0;
    var $limit   = 20;
    var $perPage = 3;
    var $is_assoc = FALSE;

    var $tag_open    = '<li style="border-bottom:none;">';
    var $tag_close   = "</li>";
    var $tag_space   = "<li>...</li>";

    function __construct() {

        $this->CI =& get_instance();

    }

    function my_anchor($url,$name, $anim=false) {
        if ($anim) {
            return $this->CI->pquery->link_to_remote($name,
                array('url' => site_url().$url,
                        'beforeSend' => $this->CI->pquery->show_loading('load_image'),
                        'success' => $this->CI->pquery->hide('#load_image'),
                        'update' => $this->panel, 'type'=> 'post'));
        } else {
            $option = array('url' => site_url().$url,
                            'update' => $this->panel,
                            'type'=> 'post');
            return $this->CI->pquery->link_to_remote($name, $option);
        }
    }

    function paginate3(){
        $this->url .= ($this->is_assoc ? '/' : '/page/');

        $strPage = "";

        $i_add = floor(($this->page-1) / $this->perPage) * $this->perPage;
        $totPage = ceil($this->totRec / $this->limit);

        for($i=1; $i<=$this->perPage; $i++){
            $_page = $i+$i_add;
            if($_page <= $totPage){
                if ($_page == $this->page)
                    $strPage .= $this->tag_open .
                        '<strong><span>'.$_page.'</span></strong>' .
                        $this->tag_close;
                else
                    $strPage .= $this->tag_open.
                        $this->my_anchor($this->url.$_page, $_page, TRUE).
                        $this->tag_close;
            }
        }

        $first = $this->tag_open .
                 $this->my_anchor($this->url.'1', '&laquo;', TRUE) .
                 $this->tag_close;

        $first.= $this->tag_open .
                 $this->my_anchor($this->url.$i_add,'&lsaquo;', TRUE) .
                 $this->tag_close;

        $last = $this->tag_open .
                $this->my_anchor($this->url.($i_add+($this->perPage+1)),
                                 '&rsaquo;', TRUE).
                $this->tag_close;

        $last.= $this->tag_open .
                $this->my_anchor($this->url.$totPage,'&raquo;', TRUE).
                $this->tag_close;

        if($this->page > "1") {
            $strPage = $this->tag_open.
                       $this->my_anchor($this->url.($this->page-1),'prev',TRUE) .
                       $this->tag_close . $strPage;
        }

        if($this->page < $totPage)
            $strPage = $strPage.$this->tag_open .
                       $this->my_anchor($this->url.($this->page+1),'next',true).
                       $this->tag_close;

        if($i_add) $strPage = $first.$strPage;
        if($this->perPage+$i_add < $totPage) $strPage = $strPage.$last;

        return $strPage;
    }

    function paginate1($url=null,$page=1, $totRec=0, $recPerPage=20, $range=2, $is_assoc=0){
        if(!$is_assoc){
            $url .= '/page/';
        }else{
            $url .= '/';
        }

        if (is_numeric($totRec)) {

            $nav = '';

            $totPage = ceil($totRec/$recPerPage);
            $min = $page - $range;
            $min = $min < 1 ? 1 : $min;

            if ($min > 1) {
                $nav .= $this->tag_open . $this->my_anchor($url.'1', '1', true) . $this->tag_close;
                $nav .= $this->tag_space;
            }

            $max = $page + $range;
            $max = $max > $totPage ? $totPage : $max;
            for ($i=$min; $i<=$max; $i++) {
                if ($i==$page )
                    $nav .= $this->tag_open . "<strong><span>$i</span></strong>" .$this->tag_close;
                else
                    $nav .= $this->tag_open . $this->my_anchor($url.$i, $i, true) . $this->tag_close;
            }


            if (($totPage > $range) && ($page < ($totPage-$range))) {
                $nav .= $this->tag_space;
                $nav .= $this->tag_open . $this->my_anchor($url.$totPage, $totPage, true) . $this->tag_close;
            }

            return $nav;

        } else return ' kehed sia';

    }

    function paginate2($url=null,$page=1,$totRec=0,$recPerPage=20,$linkPerPage=3,$is_assoc=0){
        if(!$is_assoc){
            $url .= '/page/';
        }else{
            $url .= '/';
        }

        $strPage = "";

        $i_add = floor(($page-1)/$linkPerPage)*$linkPerPage;
        $totPage = ceil($totRec/$recPerPage);

        for($i=1;$i<=$linkPerPage;$i++){
            $_page = $i+$i_add;
            if($_page <= $totPage){
                if ($_page == $page)
                    $strPage .= $this->tag_open . '<strong><span>'.$_page.'</span></strong>'.$this->tag_close;
                else
                    $strPage .= $this->tag_open.$this->my_anchor($url.$_page, $_page, true).$this->tag_close;
            }
        }

        $first = $this->tag_open . $this->my_anchor($url.'1', '&laquo;',true) . $this->tag_close;
        $first.= $this->tag_open . $this->my_anchor($url.$i_add,'&lsaquo;',true) . $this->tag_close;

        $last = $this->tag_open . $this->my_anchor($url.($i_add+($linkPerPage+1)),'&rsaquo;',true). $this->tag_close;
        $last.= $this->tag_open . $this->my_anchor($url.$totPage,'&raquo;',true).$this->tag_close;

        if($page > "1") $strPage = $this->tag_open.$this->my_anchor($url.($page-1),'prev',true) . $this->tag_close . $strPage;
        if($page < $totPage) $strPage = $strPage.$this->tag_open.$this->my_anchor($url.($page+1),'next',true) . $this->tag_close;

        if($i_add) $strPage = $first.$strPage;
        if($linkPerPage+$i_add < $totPage) $strPage = $strPage.$last;

        return $strPage;
    }

}

?>

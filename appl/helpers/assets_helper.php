<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Code Igniter
*
* An open source application development framework for PHP 4.3.2 or newer
*
* @package		CodeIgniter
* @author		Rick Ellis
* @modify		Enang Yusup
* @copyright	Copyright (c) 2006, pMachine, Inc.
* @license		http://www.codeignitor.com/user_guide/license.html
* @link			http://www.codeigniter.com
* @since        Version 1.0
* @filesource
*/

// ------------------------------------------------------------------------

/**
* Code Igniter Asset Helpers
*
* @package		CodeIgniter
* @subpackage	Helpers
* @category		Helpers
* @author       Philip Sturgeon < phil.sturgeon@styledna.net >
*/

// ------------------------------------------------------------------------


/**
  * General Asset Helper
  *
  * Helps generate links to asset files of any sort. Asset type should be the
  * name of the folder they are stored in.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    the asset type (name of folder)
  * @param		string    optional, module name
  * @return		string    full url to asset
  */

function other_asset_url($asset_name, $module_name = NULL, $asset_type = NULL , $ext = NULL)
{
	$obj =& get_instance();
	$base_url = base_url();

	$asset_location = $base_url.'assets/';

	if(!empty($module_name)):
		$asset_location .= 'modules/'.$module_name.'/';
	endif;

	if ($ext == NULL)
		$asset_location .= $asset_type.'/'.$asset_name;
	else
		$asset_location = $asset_type.'/'.$asset_name;

	return $asset_location;

}


// ------------------------------------------------------------------------

/**
  * Parse HTML Attributes
  *
  * Turns an array of attributes into a string
  *
  * @access		public
  * @param		array		attributes to be parsed
  * @return		string 		string of html attributes
  */

function _parse_asset_html($attributes = NULL)
{

	if(is_array($attributes)):
		$attribute_str = '';

		foreach($attributes as $key => $value):
			$attribute_str .= ' '.$key.'="'.$value.'"';
		endforeach;

		return $attribute_str;
	endif;

	return '';
}

// ------------------------------------------------------------------------

/**
  * CSS Asset Helper
  *
  * Helps generate CSS asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @return		string    full url to css asset
  */

function css_asset_url($asset_name, $module_name = NULL)
{
	return other_asset_url($asset_name, $module_name, 'css');
}


// ------------------------------------------------------------------------

/**
  * CSS Asset HTML Helper
  *
  * Helps generate JavaScript asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @param		string    optional, extra attributes
  * @return		string    HTML code for JavaScript asset
  */

function css_asset($asset_name, $module_name = NULL, $attributes = array())
{
	$attribute_str = _parse_asset_html($attributes);

	return '<link href="'.css_asset_url($asset_name, $module_name).'" rel="stylesheet" type="text/css"'.$attribute_str.' />'."\n";
}

// ------------------------------------------------------------------------

/**
  * Icon Asset Helper
  *
  * Helps generate Icon asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @return		string    full url to icon asset
  */

function icon_asset_url($asset_name, $module_name = NULL)
{
	return other_asset_url($asset_name, $module_name, 'images/icon');
}


// ------------------------------------------------------------------------

/**
  * Icon Asset HTML Helper
  *
  * Helps generate JavaScript asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @param		string    optional, extra attributes
  * @return		string    HTML code for JavaScript asset
  */

function icon_asset($asset_name, $module_name = NULL, $attributes = array())
{
	$attribute_str = _parse_asset_html($attributes);
	$temp = '<link href="'.icon_asset_url($asset_name, $module_name).'" rel="icon" type="image/x-icon"'.$attribute_str.' />'."\n";
	$temp.= '<link href="'.icon_asset_url($asset_name, $module_name).'" rel="shortcut icon" type="image/x-icon"'.$attribute_str.' />'."\n";
	return $temp;
}

// ------------------------------------------------------------------------

/**
  * Image Asset Helper
  *
  * Helps generate CSS asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @return		string    full url to image asset
  */

function image_asset_url($asset_name, $module_name = NULL)
{
	return other_asset_url($asset_name, $module_name, 'images');
}


// ------------------------------------------------------------------------

/**
  * Image Asset HTML Helper
  *
  * Helps generate image HTML.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @param		string    optional, extra attributes
  * @return		string    HTML code for image asset
  */

function image_asset($asset_name, $module_name = '', $attributes = array())
{
	$attribute_str = _parse_asset_html($attributes);

	return '<img src="'.image_asset_url($asset_name, $module_name).'"'.$attribute_str.' />'."\n";
}


// ------------------------------------------------------------------------

/**
  * Swf Asset URL Helper
  *
  * Helps generate Swf asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @return		string    full url to JavaScript asset
  */

function swf_asset_url($asset_name, $module_name = NULL)
{
	return other_asset_url($asset_name, $module_name, 'swf');
}


// ------------------------------------------------------------------------

/**
  * JavaScript Asset URL Helper
  *
  * Helps generate JavaScript asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @return		string    full url to JavaScript asset
  */

function js_asset_url($asset_name, $module_name = NULL)
{
	return other_asset_url($asset_name, $module_name, 'js');
}


// ------------------------------------------------------------------------

/**
  * JavaScript Asset HTML Helper
  *
  * Helps generate JavaScript asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @return		string    HTML code for JavaScript asset
  */

function js_asset($asset_name, $module_name = NULL)
{
	return '<script src="'.js_asset_url($asset_name, $module_name).'"></script>'."\n";
}

// ------------------------------------------------------------------------

/**
  * ExtJS JavaScript Asset URL Helper
  *
  * Helps generate ExtJS JavaScript asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @return		string    full url to JavaScript asset
  */

function ext_js_asset_url($asset_name, $module_name = NULL)
{
	return other_asset_url($asset_name, $module_name, '/ext3.0.0','ext');
}


// ------------------------------------------------------------------------

/**
  * ExtJS JavaScript Asset HTML Helper
  *
  * Helps generate ExtJS JavaScript asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @return		string    HTML code for JavaScript asset
  */

function ext_js_asset($asset_name, $module_name = NULL)
{
	return '<script src="'.ext_js_asset_url($asset_name, $module_name).'"></script>'."\n";
}

// ------------------------------------------------------------------------

/**
  * ExtJS CSS Asset Helper
  *
  * Helps generate ExtJS CSS asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @return		string    full url to css asset
  */

function ext_css_asset_url($asset_name, $module_name = NULL)
{
	return other_asset_url($asset_name, $module_name, '/ext3.0.0/resources/css','ext');
}


// ------------------------------------------------------------------------

/**
  * ExtJS CSS Asset HTML Helper
  *
  * Helps generate ExtJS CSS asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, module name
  * @param		string    optional, extra attributes
  * @return		string    HTML code for JavaScript asset
  */

function ext_css_asset($asset_name, $module_name = NULL, $attributes = array())
{
	$attribute_str = _parse_asset_html($attributes);

	return '<link href="'.ext_css_asset_url($asset_name, $module_name).'" rel="stylesheet" type="text/css"'.$attribute_str.' />'."\n";
}

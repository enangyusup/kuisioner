<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Responden extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('responden_model', 'master_model'));
        $this->load->library('form_validation');
    }

    function index() {
        $data['title'] = 'Halaman Master Responden';
        $data['rows'] = $this->responden_model->get();
        $this->load->view('master/responden', $data);
    }

    function form($master_id = 0) {
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('usia','Usia', 'trim|required|numeric');
        $this->form_validation->set_rules('anak','Anak', 'trim|required|numeric');

        if ($this->form_validation->run()) {
            $this->responden_model->master_id = $master_id;
            $this->responden_model->usia = $this->input->post('usia');
            $this->responden_model->pendidikan = $this->input->post('pendidikan');
            $this->responden_model->status = $this->input->post('status');
            $this->responden_model->jum_anak = $this->input->post('anak');
            $this->responden_model->ket = $this->input->post('ket');
            $this->responden_model->save();
            redirect('responden');
		} else {
			$this->master_model->id = $master_id;
			$data['master'] = $this->master_model->get();
            $this->load->view('master/responden_form', $data);
		}
    }

    function edit($id=0) {
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('usia','Usia', 'trim|required|numeric');
        $this->form_validation->set_rules('anak','Anak', 'trim|required|numeric');

        if ($this->form_validation->run()) {
            $this->responden_model->id = $id;
            $this->responden_model->master_id = $this->input->post('master_id');
            $this->responden_model->usia = $this->input->post('usia');
            $this->responden_model->pendidikan = $this->input->post('pendidikan');
            $this->responden_model->status = $this->input->post('status');
            $this->responden_model->jum_anak = $this->input->post('anak');
            $this->responden_model->ket = $this->input->post('ket');
            $this->responden_model->save();
            redirect('responden');
		} else {
			$this->responden_model->id = $id;
			$row = $this->responden_model->get();
            $data['row'] = $row;
            $this->load->view('master/responden_edit', $data);
		}
    }

    function delete($id) {
        $this->responden_model->id = $id;
        $this->responden_model->delete();
        redirect('responden');
    }

}

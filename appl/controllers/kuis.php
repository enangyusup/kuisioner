<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kuis extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('kuis_model',
                                 'responden_model',
                                 'pertanyaan_model',
                                 'pertanyaandet_model'));
        $this->load->library('form_validation');
    }

    function index() {
        $data['title'] = 'Halaman Kuisioner';
        $data['rows'] = $this->responden_model->get();
        $this->load->view('kuis', $data);
    }

    function form($r_id=0, $p_id=0) {
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('soal','Soal', 'trim|required');


        //jika p_id belum ada
		if ($p_id==0) {
			$data['rows'] = $this->pertanyaan_model->get();
			$this->load->view('master/kuis_form', $data);
		} else {
			// get 1 pertanyaan pertama
			$this->pertanyaan_model->limit=1;
			$query = $this->pertanyaan_model->get();
			$p_id = $p_id ? $p_id : $query['id'];

			if ($this->form_validation->run()) {
				$this->kuis_model->id = $id;
				$this->kuis_model->soal = $this->input->post('soal');
				$this->kuis_model->ket = $this->input->post('ket');
				$this->kuis_model->save();
				redirect('kuis');
			} else {
				$data['pertanyaan'] = $this->pertanyaan_model->get();

				$this->pertanyaandet_model->p_id = $p_id;
				$data['pertanyaandet'] = $this->pertanyaandet_model->get();
				//print "<pre>"; print_r($data); die;
				$this->load->view('master/kuis_formdet', $data);
			}
		}
    }

    function delete($id) {
        $this->kuis_model->id = $id;
        $this->kuis_model->delete();
        redirect('kuis');
    }

}

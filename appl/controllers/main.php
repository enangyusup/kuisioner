<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('master_model');
    }

    function index() {
        $data['title'] = 'Halaman Utama';
        $data['rows'] = $this->master_model->get();
        $this->load->view('home', $data);
    }

    function random() {
        $data['title'] = 'Random Data';
        $this->master_model->limit = 1;
        $this->master_model->random = TRUE;
        $row = $this->master_model->get();
        if (!empty($row['no_telp'])) {
            $result = $row['id'].'#'.trim($row['no_telp']).'#'.trim($row['nama']);
        } else {
            $result = '-';
        }
        echo $result;
    }


}

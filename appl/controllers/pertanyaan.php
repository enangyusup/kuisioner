<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pertanyaan extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('pertanyaan_model');
        $this->load->library('form_validation');
    }

    function index() {
        $data['title'] = 'Halaman Master Pertanyaan';
        $data['rows'] = $this->pertanyaan_model->get();
        $this->load->view('master/pertanyaan', $data);
    }

    function form($id=0) {
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('soal','Soal', 'trim|required');

        if ($this->form_validation->run()) {
            $this->pertanyaan_model->id = $id;
            $this->pertanyaan_model->soal = $this->input->post('soal');
            $this->pertanyaan_model->ket = $this->input->post('ket');
            $this->pertanyaan_model->save();
            redirect('pertanyaan');
		} else {
            $data['id'] = $id;
            $this->pertanyaan_model->id = $id;
            $data['row'] = $this->pertanyaan_model->get();
            //echo "<pre>"; print_r($data['row']); die();
            $this->load->view('master/pertanyaan_form', $data);
		}
    }

    function delete($id) {
        $this->pertanyaan_model->id = $id;
        $this->pertanyaan_model->delete();
        redirect('pertanyaan');
    }

}

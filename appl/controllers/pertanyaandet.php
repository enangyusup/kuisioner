<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pertanyaandet extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('pertanyaandet_model');
        $this->load->library('form_validation');
    }

    function index($p_id=0) {
        $data['title'] = 'Halaman Master Detail Pertanyaan';
		$data['p_id'] = $p_id;
		$this->pertanyaandet_model->p_id = $p_id;
        $data['rows'] = $this->pertanyaandet_model->get();
        $this->load->view('master/pertanyaandet', $data);
    }

    function form($p_id=0, $id=0) {
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('soal','Soal', 'trim|required');

        if ($this->form_validation->run()) {
            $this->pertanyaandet_model->id = $id;
            $this->pertanyaandet_model->p_id = $p_id;
            $this->pertanyaandet_model->soal = $this->input->post('soal');
            $this->pertanyaandet_model->ket = $this->input->post('ket');
            $this->pertanyaandet_model->save();
            redirect('pertanyaandet/index/'.$p_id);
		} else {
            $data['id'] = $id;
            $this->pertanyaandet_model->id = $id;
            $data['row'] = $this->pertanyaandet_model->get();
            $this->load->view('master/pertanyaandet_form', $data);
		}
    }

    function delete($p_id=0, $id=0) {
        $this->pertanyaandet_model->id = $id;
        $this->pertanyaandet_model->delete();
        redirect('pertanyaandet/index/'.$p_id);
    }

}

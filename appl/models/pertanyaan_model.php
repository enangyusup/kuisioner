<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Pertanyaan_model extends CI_Model {

    var $id = 0;
    var $soal = NULL;
    var $ket = NULL;
    var $limit = 10;
    var $table = 'pertanyaan';

    function __construct() {
        parent::__construct();
    }

    function get() {
        $this->_prep_query();
        $query = $this->db->get();
        if ($this->id or ($this->limit == 1)) {
            if ($query->row()) {
                return $query->row_array();
            }
        } else {
            if ($query->result()) {
                return $query->result_array();
            }
        }
        return FALSE;
    }

    function save() {

        $data = array(
                    'soal' => $this->soal,
                    'ket'  => $this->ket
                );
        if ($this->id) {
			$this->db->where('id', $this->id);
			$this->db->update($this->table, $data);
		} else {
			$this->db->insert($this->table, $data);
		}

    }

	function delete() {

		$this->db->where('id', $this->id);
		$this->db->delete($this->table);

	}

    function _prep_query() {

        if ($this->id) {
            $this->db->where('id', $this->id);
        }

        $this->db->limit($this->limit);
        $this->db->order_by('id');
        $this->db->from($this->table);
    }


}

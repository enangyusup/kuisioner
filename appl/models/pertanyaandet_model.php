<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Pertanyaandet_model extends CI_Model {

    var $id = 0;
    var $p_id = 0;
    var $soal = NULL;
    var $ket = NULL;
    var $limit = 10;
    var $table1 = 'pertanyaan_detail';
    var $table2 = 'pertanyaan';

    function __construct() {
        parent::__construct();
    }

    function get() {
        $this->_prep_query();
        $query = $this->db->get();
        if ($this->id) {
            if ($query->row()) {
                return $query->row_array();
            }
        } else {
            if ($query->result()) {
                return $query->result_array();
            }
        }
        return FALSE;
    }

    function save() {

        $data = array(
                    'p_id' => $this->p_id,
                    'soal' => $this->soal,
                    'ket'  => $this->ket
                );
        if ($this->id) {
			$this->db->where('id', $this->id);
			$this->db->update($this->table1, $data);
		} else {
			$this->db->insert($this->table1, $data);
		}

    }

	function delete() {

		$this->db->where('pertanyaan_detail.id', $this->id);
		$this->db->delete($this->table1);

	}

    function _prep_query() {

        $this->db->select('pertanyaan_detail.*,pertanyaan.soal as p_soal');
		if ($this->id) {
            $this->db->where('pertanyaan_detail.id', $this->id);
        }

        if ($this->p_id) {
            $this->db->where('pertanyaan_detail.p_id', $this->p_id);
        }

        $this->db->limit($this->limit);
        $this->db->from($this->table1);
		$this->db->join($this->table2, 'pertanyaan.id = pertanyaan_detail.p_id');
    }


}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Master_model extends CI_Model {

    var $id = NULL;
    var $no_telp = NULL;
    var $nama = NULL;
    var $random = FALSE;
    var $limit = 10;

    function __construct() {
        parent::__construct();
    }

    function get() {
        $query = $this->_prep_query();
        if ($this->random or $this->id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
        }
        return 0;
    }

    function save() {
        $data = array(
                    'no_telp' => $this->no_telp,
                    'nama' => $this->nama
                );
        if ($this->id) {
			$this->db->where('id', $this->id);
			$this->db->update($this->table, $data);
		} else {
			$this->db->insert($this->table, $data);
		}
    }

	function delete() {
		$this->db->where('id', $this->id);
		$this->db->delete($this->table);
	}

    function _prep_query() {
        if ($this->id) {
            $query = '
                SELECT * FROM master_telpon
                WHERE id = '. $this->id;
        } else {
            $query = '
                SELECT * FROM master_telpon
                WHERE id NOT IN (SELECT DISTINCT(master_id) FROM responden)
                ORDER BY '.($this->random ? 'RAND()' : 'nama').'
                LIMIT '.$this->limit;
        }

        return $this->db->query($query);
    }


}

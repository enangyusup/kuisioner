<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Responden_model extends CI_Model {

    var $id = 0;
    var $master_id = NULL;
    var $usia = NULL;
    var $pendidikan = NULL;
    var $status = NULL;
    var $jum_anak = NULL;
    var $limit = 10;
    var $table1 = 'responden';
    var $table2 = 'master_telpon';

    function __construct() {
        parent::__construct();
    }

    function get() {
        $this->_prep_query();
        $query = $this->db->get();
        if ($this->id) {
            if ($query->row()) {
                return $query->row_array();
            }
        } else {
            if ($query->result()) {
                return $query->result_array();
            }
        }
        return FALSE;
    }

    function save() {

        $data = array(
                    'master_id'  => $this->master_id,
                    'usia' 		 => $this->usia,
                    'pendidikan' => $this->pendidikan,
                    'status' 	 => $this->status,
                    'jum_anak' 	 => $this->jum_anak,
                    'ket'  		 => $this->ket
                );
        if ($this->id) {
			$this->db->where('id', $this->id);
			$this->db->update($this->table1, $data);
		} else {
			$this->db->insert($this->table1, $data);
		}

    }

	function delete() {

		$this->db->where('id', $this->id);
		$this->db->delete($this->table1);

	}

    function _prep_query() {

		$this->db->select('a.*, b.nama, b.no_telp');
		$this->db->from($this->table1.' AS a');
		$this->db->join($this->table2.' AS b', 'a.master_id = b.id');
		if ($this->id) {
            $this->db->where('a.id', $this->id);
        }

        $this->db->limit($this->limit);
    }


}

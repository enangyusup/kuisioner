<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Kuis_model extends CI_Model {

    var $id = NULL;
    var $p_id = NULL;
    var $r_id = NULL;
    var $table1 = 'kuis';
    var $table2 = 'pertanyaan_detail';
    var $table3 = 'responden';
    var $table4 = 'responden';
    var $limit = 10;

    function __construct() {
        parent::__construct();
    }

    function get() {
        $query = $this->_prep_query();
        if ($this->id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }
        }
        return 0;
    }

    function save() {
        $data = array(
                    'pertanyaan_id' => $this->p_id,
                    'responden_id'  => $this->r_id
                );
        if ($this->id) {
			$this->db->where('id', $this->id);
			$this->db->update($this->table1, $data);
		} else {
			$this->db->insert($this->table1, $data);
		}
    }

	function delete() {
		$this->db->where('id', $this->id);
		$this->db->delete($this->table);
	}

    function _prep_query() {
        $query = '
            SELECT a.*, b.soal, d.nama FROM kuis AS a
            LEFT JOIN pertanyaan_detail AS b ON (a.pertanyaan_id = b.id)
            LEFT JOIN responden AS c ON (a.responden_id = c.id)
            LEFT JOIN master_telpon AS d ON (c.master_id = d.id)
        ';

        if ($this->id) {
            $query .= ' WHERE a.id = '.$this->id;
        } else {
            $query .= ' ORDER BY a.id DESC LIMIT '.$this->limit;
        }

        return $this->db->query($query);
    }


}

<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['game_terbaru']     = "New Game";
$lang['game_10_terbaik']  = "Top 10 Games";
$lang['game_donlot']      = "Download";
$lang['game_more']        = "More";
$lang['game_donlot_game'] = "Try It Now";
$lang['game_ukuran']      = "Size";
$lang['game_beli']        = "Buy Now";
$lang['game_desc_plus']   = "More";
$lang['game_desc_min']    = "Less";
$lang['game_back']        = "Back";
$lang['game_bahasa']      = "Language";
$lang['game_screenshots'] = "Screenshots";
$lang['game_fitur']       = "Key Game Features";
$lang['game_syarat']      = "System Requirement";
$lang['game_play']        = "Play Online";
